***
      PROGRAM popsyn
***
*
* Evolves a population of binaries using input parameters 
* read from input file binaries.in (M1, M2, P, e, Z, Tmax). 
*
***
      implicit none
*
      INCLUDE '../input/const_mobse.h'
*
      integer i,j,k,kk,jj,nsyst,met,countcheck,nsyst2sim
      integer kw,kw2,kwx,kwx2,kstar(2)
      integer i1,i2,kdum,id
*
      real*8 m1,m2,tmax,t,kick1,kick2,cmu1,cmu2
      real*8 Vcm(6),aspinBH(2)
      real*8 t_SN1,t_SN2,t_merg
      LOGICAL cob,rem3,SN1,SN2
      integer formk1,formk2
      real*8 formtime,formsep,formecc,formm1,formm2
      real*8 mass0(2),mass(2),z,zpars(20)
      real*8 epoch(2),tms(2),tphys,tphysf,dtp
      real*8 rad(2),lum(2),ospin(2)
      real*8 massc(2),radc(2),menv(2),renv(2)
      real*8 sep0,tb0,tb,ecc0,ecc,aursun,yeardy,yearsc,tol
      PARAMETER(aursun=214.95d0,yeardy=365.25d0,yearsc=3.1557d+07)
      PARAMETER(tol=1.d-07)
      CHARACTER*8 label(14)
      CHARACTER*9 species(4)
      real*8 t1,t2,mx,mx2,tbx,eccx
*
************************************************************************
* MOBSE parameters:
*
* mass is in solar units.
* tphysf is the maximum evolution time in Myr.
* tb is the orbital period in days.
* kstar is the stellar type: 0 or 1 on the ZAMS - unless in evolved state. 
* z is metallicity in the range 0.0001 -> 0.03 where 0.02 is Population I.
* eccentricity can be anywhere in the range 0.0 -> 1.0.
*
* neta is the Reimers mass-loss coefficent (neta*4x10^-13: 0.5 normally). 
* bwind is the binary enhanced mass loss parameter (inactive for single).
* hewind is a helium star mass loss factor (1.0 normally, inactive in MOBSE1).
* alpha1 is the common-envelope efficiency parameter (1.0).  
* lambda is the binding energy factor for common envelope evolution (0.1).
*
* ceflag > 0 activates spin-energy correction in common-envelope (0). #defunct#
* ceflag = 3 activates de Kool common-envelope model (0). 
* tflag > 0 activates tidal circularisation (1).
* ifflag > 0 uses WD IFMR of HPE, 1995, MNRAS, 272, 800 (0). 
* wdflag > 0 uses modified-Mestel cooling for WDs (1). 
* bhflag > 0 allows velocity kick at BH formation (1). 
* nsflag > 0 takes NS/BH mass from (default 3): 
*	   1 --> Belczynski et al. 2002, ApJ, 572, 407
*	   2 --> Rapid supernova model Fryer et al. 2012, ApJ, 749, 91
*        3 --> Delayed supernova model Fryer et al. 2012, ApJ, 749, 91
* piflag > 0 activates the PPISNs and PISNe (1) Spera et al. 2017, MNRAS, 470, 4739.  
* spinflag decides the spin model (now is WIP)
* baveraflag if 0 does nothing, if 1 introduces the corrections of Bavera+2021
* mxns is the maximum NS mass (3.0).
* idum is the random number seed used by the kick routine. 
*
* Next come the parameters that determine the timesteps chosen in each
* evolution phase:
*                 pts1 - MS                  (0.05) 
*                 pts2 - GB, CHeB, AGB, HeGB (0.01)
*                 pts3 - HG, HeMS            (0.02)
* as decimal fractions of the time taken in that phase.
*
* sigma1 is the dispersion in the Maxwellian for the SN kick speed (265. km/s)
*            from Hobbs et al. 2005, ApJ, 591, 288. 
* sigma2 is the dispersion in the Maxwellian for the SN kick speed (26. km/s)
*            to considere the different mechanims involve in ECS. 
* beta is wind velocity factor: proportional to vwind**2 (1/8). 
* xi is the wind accretion efficiency factor (1.0). 
* acc2 is the Bondi-Hoyle wind accretion factor (3/2). 
* epsnov is the fraction of accreted matter retained in nova eruption (0.001). 
* eddfac is Eddington limit factor for mass transfer (1.0).
* gamma is the angular momentum factor for mass lost during Roche (-1.0). 
*
      OPEN(16,file='../input/popsyn_const.in', status='old')
      READ(16,*)neta,bwind,hewind,alpha1,lambda
      READ(16,*)ceflag,tflag,ifflag,wdflag,bhflag,nsflag,piflag,
     &          spinflag,baveraflag,mxns,idum
      READ(16,*)pts1,pts2,pts3
      READ(16,*)sigma1,sigma2,beta,xi,acc2,epsnov,eddfac,gamma
*
* Set the seed for the random number generator. 
*
      if(idum.gt.0) idum = -idum
*
* Set the collision matrix.
*
      CALL instar

      label(1) = 'INITIAL '
      label(2) = 'KW_CHNGE'
      label(3) = 'BEG_RCHE'
      label(4) = 'END_RCHE'
      label(5) = 'CONTACT '
      label(6) = 'COELESCE'
      label(7) = 'COMENV  '
      label(8) = 'GNTAGE  '
      label(9) = 'NO_REMNT'
      label(10) = 'MAX_TIME'
      label(11) = 'DISRUPT '
      label(12) = 'BEG_SYMB'
      label(13) = 'END_SYMB'
      label(14) = 'BEG_BSS '
*
      species(1) = 'SINGLE   '
      species(2) = 'NO_MERGER'
      species(3) = 'MERGER   '
      species(4) = 'GWS_EVENT'
*
* Open the input file - list of binary initial parameters. 
*
      OPEN(10,file='../input/popsyn.in', status='old')
      READ(10,*)nsyst
*
* Open the output files. 
*
      OPEN(11,file='../params.out', status='unknown')
      OPEN(12,file='../evol_mergers.out',status='unknown')
      OPEN(13,file='../COB.out',status='unknown')
      OPEN(14,file='../mergers.out',status='unknown')
      OPEN(15,file='../CO.out',status='unknown')
      OPEN(17,file='../failed_systems.out',status='unknown')
*
      WRITE(11,122)neta,bwind,hewind,alpha1,lambda
      WRITE(11,123)ceflag,tflag,ifflag,wdflag,bhflag,nsflag,piflag,
     &           spinflag,baveraflag,mxns,-idum
      WRITE(11,124)pts1,pts2,pts3
      WRITE(11,125)sigma1,sigma2,beta,xi,acc2,epsnov,eddfac,gamma
      WRITE(11,126)nsyst
 121  FORMAT(f6.4,2x,f8.2)
 122  FORMAT(3f4.1,f5.2,f4.1)
 123  FORMAT(9i2,2x,f5.1,i7)
 124  FORMAT(3f5.2)
 125  FORMAT(2f6.1,f6.3,2f5.1,f6.3,f5.1,f5.1)
 126  FORMAT(i10)
      
*12 = evol mergers
      WRITE(12,*)'#ID[0]  t_step[1]   k1[2]   m01[3]   mt1[4]',
     &      '  logL1[5]   logR1[6]   logT1[7]   mc1[8]   rc1[9]',
     &       '   menv1[10]   renv1[11]   epoch1[12]   ospin1[13]',
     &     '   dmt1[14]   r1/rol1[15]   k2[16]   m02[17]   mt2[18]',
     &     '   logL2[19]   logR2[20]   logT2[21]   mc2[22]   rc2[23]',
     &       '   menv2[24]   renv2[25]   epoch2[26]   ospin2[27]',
     &     '   dmt2[28]   r2/rol2[29]   tb[30]   sep[31]   ecc[32]',
     &     '   label[33] BHspin1[34] BHspin2[35]'
* 13 = COB.out
      WRITE(13,*)'#ID[0]   min1[1]   min2[2]   kick1[3]  kick2[4]',   
     &       '   cmu1[5]  cmu2[6] tform[7]   sepform[8]',
     &           '   eccform[9]   k1form[10]   m1form[11]',
     &       '   k2form[12]   m2form[13]   label[14]  V1cmX[15]',  
     &     '  V1cmY[16] V1cmZ[17]  V2cmX[18]  V2cmY[19] V2cmZ[20]',
     &     ' t_SN1[21]  t_SN2[22] BHspin1[23] BHspin2[24]'
* 14 = mergers.out
      WRITE(14,*)'#ID[0]   min1[1]   min2[2]  kick1[3]  kick2[4]',   
     &       '   cmu1[5]  cmu2[6],  tform[7]   sepform[8]',
     &           '   eccform[9]   k1form[10]   m1form[11]',
     &       '   k2form[12]   m2form[13]   tmerg[14]   k1[15]   m1[16]',
     &   '   k2[17]   m2[18]   sep[19]   ecc[20]  label[21]  V1cmX[22]',  
     &     '  V1cmY[23] V1cmZ[24]  V2cmX[25]  V2cmY[26] V2cmZ[27]',
     &     ' t_SN1[28]  t_SN2[29] BHspin1[30] BHspin2[31]'
* 15 = CO.out
      WRITE(15,*)'#ID[0]  min[1]  mrem[2]  kw[3]  kick[4]',  
     &           '  cmu[5]  time[6]  BHspin[7] species[8]'
*

      countcheck = 0
      i = 1
*
      do while(i.le.nsyst)
*
* Read in parameters and set coefficients which depend on metallicity. 
*
         READ(10,*)id,m1,m2,tb,ecc,z,tmax
*
* Set the value on true to save the simulation of the system.
*
         saveflag = .true.
*
         CALL zcnsts(z,zpars)
*
C------------------------------
c For single stellar evolution
c         m2 = 0.0
c         tb = 0.0
c         ecc = 0.0
C------------------------------
         ecc0 = ecc
         tb0 = tb/yeardy
         sep0 = aursun*(tb0*tb0*(mass(1) + mass(2)))**(1.d0/3.d0)
         tb0 = tb
*
* Initialize the binary. 
*
         if(m1 .le. 0.7)then
             kstar(1) = 0
         else
            kstar(1) = 1
         endif
         mass0(1) = m1
         mass(1) = m1
         massc(1) = 0.0
         ospin(1) = 0.0
         epoch(1) = 0.0
*
         if(m2 .le. 0.7)then
             kstar(2) = 0
         else
            kstar(2) = 1
         endif
         mass0(2) = m2
         mass(2) = m2
         massc(2) = 0.0
         ospin(2) = 0.0
         epoch(2) = 0.0
*
         tphys = 0.0
         tphysf = tmax
         dtp = 0.0
*
* Evolve the binary. 
*
         CALL evolv2(kstar,mass0,mass,rad,lum,massc,radc,
     &               menv,renv,ospin,epoch,tms,
     &               tphys,tphysf,dtp,z,zpars,tb,ecc)
*
* If the mass exceed 150 solar masses go to the next system
*
         if(saveflag.eqv..false.)then
            countcheck = countcheck + 1
            WRITE(17,111)id,m1,m2,tb0,ecc0,z,tmax
            cycle
         endif
*
* Else save the input
*
         WRITE(11,111)id,m1,m2,tb0,ecc0,z,tmax
 111     FORMAT(i8,2f9.4,f16.4,f8.4,f7.4,f8.1)
* --------------------
* CREATE evolution.out
* --------------------
c         jj = 1   
c         do while(bpp(jj,1).ge.0.0d0)
c            kstar(1) = INT(bpp(jj,2))
c            kstar(2) = INT(bpp(jj,16))
c            kw = INT(bpp(jj,33))
c            WRITE(12,112)i,bpp(jj,1),kstar(1),(bpp(jj,kk),kk=3,15),
c     &        kstar(2),(bpp(jj,kk),kk=17,32),label(kw)
c            jj = jj + 1
c         enddo
*
         jj = 1
         cob = .false.   
         rem3 = .false.
         SN1 = .false.
         SN2 = .false.
         do while(bpp(jj,1).ge.0.0d0)
            kstar(1) = INT(bpp(jj,2))
            kstar(2) = INT(bpp(jj,16))
            kw = INT(bpp(jj,33))

* Save time at which occur SN1
            if((kstar(1).eq.14 .or. kstar(1).eq.13) .and. 
     &           (SN1.eqv..false.))then
                  SN1 = .true.
                  t_SN1 = bpp(jj,1)
            endif
* Save time at which occur SN2
            if((kstar(2).eq.14 .or. kstar(2).eq.13) .and. 
     &           (SN2.eqv..false.))then
                  SN2 = .true.
                  t_SN2 = bpp(jj,1)
            endif
* --------------------
* CREATE COB.out
* --------------------
            if(((kstar(1).eq.14 .or. kstar(1).eq.13) .and. 
     &        (kstar(2).eq.14 .or. kstar(2).eq.13)) .and.
     &        (bpp(jj,32).ge.0.d0.and.bpp(jj,32).lt.1.d0) .and.
     &        (bpp(jj,31).gt.0.d0) .and. (cob.eqv..false.))then
                formtime = bpp(jj,1)
                formecc = bpp(jj,32)
                formsep = bpp(jj,31)
                formk1 = kstar(1)
                formk2 = kstar(2)
                formm1 = bpp(jj,4)
                formm2 = bpp(jj,18)
                kick1 = bpp(jj,37)
                kick2 = bpp(jj,42)
                cmu1 = bpp(jj,38)
                cmu2 = bpp(jj,43)
                Vcm(1) = bpp(jj,44)
                Vcm(2) = bpp(jj,45) 
                Vcm(3) = bpp(jj,46) 
                Vcm(4) = bpp(jj,47) 
                Vcm(5) = bpp(jj,48) 
                Vcm(6) = bpp(jj,49)
                aspinBH(1) = bpp(jj,50)
                aspinBH(2) = bpp(jj,51)
                cob = .true.
                write(13,113)id,m1,m2,kick1,kick2,
     &            cmu1,cmu2,formtime,formsep,formecc,
     &               formk1,formm1,formk2,formm2,label(kw),
     &               Vcm(1),Vcm(2),Vcm(3),Vcm(4),Vcm(5),Vcm(6),
     &               t_SN1,t_SN2,aspinBH(1),aspinBH(2)
            endif
*
* --------------------
* CREATE mergers.out
* --------------------
            if(((kstar(1) .eq. 15 .and. (kstar(2) .eq. 14 .or.
     &        kstar(2) .eq. 13)) .or. 
     &        (kstar(2) .eq. 15 .and. (kstar(1) .eq. 14 .or.
     &        kstar(1) .eq. 13))) .and.
     &        (cob.eqv..true.))then
               t_merg = bpp(jj,1)
               write(14,114) id,m1,m2,kick1,kick2,
     &            cmu1,cmu2,formtime,formsep,formecc,
     &           formk1,formm1,formk2,formm2,
     &           t_merg,kstar(1),bpp(jj,4),
     &              kstar(2),bpp(jj,18),(bpp(jj,kk),kk=31,32),label(kw),
     &              (bpp(jj,kk),kk=44,49),t_SN1,t_SN2,
     &              bpp(jj,50),bpp(jj,51)
               rem3 = .true.
               cob = .false.
*
               goto 50
            endif
*
           jj = jj + 1
         enddo
*
 50      continue
*
* --------------------
* CREATE CO.out
* --------------------
         if(rem3.eqv..true.)then
               write(15,115) id,m1,formm1,formk1,kick1,cmu1,t_SN1,
     &          aspinBH(1),species(3)
               write(15,115) id,m2,formm2,formk2,kick2,cmu2,t_SN2,
     &          aspinBH(2),species(3)
               write(15,115) id,m1,formm1+formm2,20,-1.d0,-1.d0,
     &          t_merg, 0.7, species(4)
         elseif(cob.eqv..true.)then
               write(15,115) id,m1,formm1,formk1,kick1,cmu1,t_SN1,
     &          aspinBH(1),species(2)
               write(15,115) id,m2,formm2,formk2,kick2,cmu2,t_SN2,
     &          aspinBH(2),species(2)
         else
               jj = jj - 1  
               kstar(1) = INT(bpp(jj,2))
               kstar(2) = INT(bpp(jj,16))
               if(kstar(1).eq.14 .or. kstar(1).eq.13)then
                  write(15,115) id,m1,bpp(jj,4),kstar(1),
     &                 bpp(jj,37),bpp(jj,38),t_SN1,
     &                 bpp(jj,50),species(1)
               endif
               if(kstar(2).eq.14 .or. kstar(2).eq.13)then
                  write(15,115) id,m2,bpp(jj,18),kstar(2),
     &                 bpp(jj,42),bpp(jj,43),t_SN2,
     &                 bpp(jj,51),species(1)
            endif
         endif
* -----------------------                        
* CREATE evol_mergers.out                                 
* -----------------------
         if(rem3.eqv..true.)then                      
            jj = 1                              
            do while(bpp(jj,1).ge.0.0d0)              
               kstar(1) = INT(bpp(jj,2))       
               kstar(2) = INT(bpp(jj,16))         
               kw = INT(bpp(jj,33))                   
               WRITE(12,112)i,bpp(jj,1),kstar(1),
     &            (bpp(jj,kk),kk=3,15),                       
     &              kstar(2),(bpp(jj,kk),kk=17,32),label(kw),
     &              (bpp(jj,kk),kk=44,49),bpp(jj,50),bpp(jj,51)
               jj = jj + 1                               
            enddo
         endif                   
*
         i = i + 1
      enddo
*
 112  FORMAT(i8,2x,e12.5,2x,i3,2x,e12.5,2x,e12.5,2x,
     &     e12.5,2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,
     &     2x,e12.5,2x,e12.5,2x,
     &     e12.5,2x,e12.5,2x,i3,2x,e12.5,2x,e12.5,2x,
     &     e12.5,2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,
     &     2x,e12.5,2x,e12.5,
     &     2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,2x,a8,
     &     2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,
     &     2x,e12.5,2x,e12.5)
 113  FORMAT(i8,2f9.4,1x,4f9.4,2x,f10.4,2x,e12.5,2x,e12.5,2x,i3,f9.4,
     &     2x,i3,f9.4,2x,a8,
     &     2x,e12.4,2x,e12.4,2x,e12.4,2x,e12.4,2x,e12.4,2x,e12.4,
     &     2x,e12.4,2x,e12.4,2x,e12.4,2x,e12.4)
 114  FORMAT(i8,2x,e12.4,2x,e12.4,2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,
     &     2x,e12.5,2x,e12.5,2x,e12.5,2x,i3,2x,e12.5,
     &     2x,i3,2x,e12.5,2x,e10.5,2x,i3,2x,e12.5,
     &     2x,i3,2x,e12.5,
     &     2x,e12.5,2x,e12.5,2x,a8,
     &     2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5,
     &     2x,e12.5,2x,e12.5,2x,e12.5,2x,e12.5)

 115  FORMAT(i8,1x,f9.5,1x,f9.5,1x,i3,2x,f12.5,2x,f9.5,2x,f12.5,
     &     2x,f9.5,2x,a9)
*
      write(*,*) "the systems systems in fort.99 are:"
      write(*,*) countcheck
      write(*,*) "total number of systems in popbin.out are:"
      write(*,*) i-1
      write(*,*) "the initial systems are:"
      write(*,*) nsyst
*
      if(i-1.ne.nsyst) WRITE(*,*) "ERROR: we have less systems"
*
* Input
      CLOSE(10)
      CLOSE(16)
* Output
      CLOSE(11)
      CLOSE(12)
      CLOSE(13)
      CLOSE(14)
      CLOSE(15)
      CLOSE(17)
*
************************************************************************
*
      STOP
      END
***
