*** Calculates WR spin up with Bavera's formula (MM+RR did on July 2021)
      SUBROUTINE WRspinup(aspin,mWR,period,kstar)
      implicit none

      INTEGER ceflag,tflag,ifflag,nsflag,wdflag
      INTEGER piflag,spinflag,baveraflag
      COMMON /FLAGS/ ceflag,tflag,ifflag,nsflag,wdflag
      COMMON /NEWFLAGS/ piflag,spinflag,baveraflag
      real*8 aspin, mWR, period
      real*8 pday,alpha,beta
      real*8 c1a,c2a,c3a,c1b,c2b,c3b
      integer kstar

*     Convert the period from years in days
      pday=period*365.25d0

*     He depletion
      c1a=0.059305
      c2a=0.035552
      c3a=0.270245
      c1b=0.026960
      c2b=0.011001
      c3b=0.420739
      alpha=-c1a/(c2a+EXP(-c3a*mWR))
      beta=-c1b/(c2b+EXP(-c3b*mWR))

      if(period.le.1)then
         aspin=alpha*(LOG10(pday))**2+beta*LOG10(pday)
      else
         aspin=0
      endif

      OPEN(65,file='./bavera_spin.out', status='unknown')
      WRITE(65,*)period,mWR,aspin,kstar


      return
      end
 
