***
      PROGRAM spectrum
***
*
* Evolves a population of binaries using input parameters 
* read from input file binaries.in (M1, M2, P, e, Z, Tmax). 
*
***
      implicit none
*
      INCLUDE '../input/const_mobse.h'
*
      integer i,j,k,jj
      integer kw1,kw2,kwx,kwx2,kstar(2),kfin
      integer i1,i2,kdum,arrayend
      character*30 name,vs,sn,me
      character*10 time
      character*8 date
      character*5 zone
      CHARACTER*8 label(14)
*
      real*8 m1,m2,tmax,msta,mend,mstep,nmass,mfin,tfin
      real*8 mass0(2),mass(2),z,zpars(20)
      real*8 epoch(2),tms(2),tphys,tphysf,dtp
      real*8 rad(2),lum(2),ospin(2)
      real*8 massc(2),radc(2),menv(2),renv(2)
      real*8 sep0,tb0,tb,ecc0,ecc,aursun,yeardy,yearsc,tol
      PARAMETER(aursun=214.95d0,yeardy=365.25d0,yearsc=3.1557d+07)
      PARAMETER(tol=1.d-07)
      real*8 t1,t2,mx,mx2,tbx,eccx
*
************************************************************************
* BSE parameters:
*
* neta is the Reimers mass-loss coefficent (neta*4x10^-13: 0.5 normally). 
* bwind is the binary enhanced mass loss parameter (inactive for single).
* hewind is a helium star mass loss factor (1.0 normally).
* alpha1 is the common-envelope efficiency parameter (1.0).  
* lambda is the binding energy factor for common envelope evolution (0.5).
*
* ceflag > 0 activates spin-energy correction in common-envelope (0). 
* tflag > 0 activates tidal circularisation (1).
* ifflag > 0 uses WD IFMR of HPE, 1995, MNRAS, 272, 800 (0). 
* wdflag > 0 uses modified-Mestel cooling for WDs (0). 
* bhflag > 0 allows velocity kick at BH formation (0). 
* nsflag > 0 takes NS/BH mass from 
*	1 --> Belczynski model in StarTrack
*	2 --> Rapid supernova model Fryer et al. 2012, ApJ, 749, 91
*   	3 --> Delayed supernova model Bethe H. A., 1990, Rev. Modern Phys., 62, 801
*	4 --> Belczynski et al. 2002, ApJ, 572, 407
*	5 --> The final mass before the remnant
* spinflag decides the model for spins (now still WIP)
*       1 --> Belczynski+Geneva model
* mxns is the maximum NS mass (1.8, nsflag=0; 3.0, nsflag=1).
* idum is the random number seed used by the kick routine. 
*
* Next come the parameters that determine the timesteps chosen in each
* evolution phase:
*                 pts1 - MS                  (0.05) 
*                 pts2 - GB, CHeB, AGB, HeGB (0.01)
*                 pts3 - HG, HeMS            (0.02)
* as decimal fractions of the time taken in that phase.
*
* sigma is the dispersion in the Maxwellian for the SN kick speed (190 km/s). 
* beta is wind velocity factor: proportional to vwind**2 (1/8). 
* xi is the wind accretion efficiency factor (1.0). 
* acc2 is the Bondi-Hoyle wind accretion factor (3/2). 
* epsnov is the fraction of accreted matter retained in nova eruption (0.001). 
* eddfac is Eddington limit factor for mass transfer (1.0).
* gamma is the angular momentum factor for mass lost during Roche (-1.0). 
*
* msta is the minimum mass of a star
* mend is the maximum mass of a star
* mstep is the mass step for the mass spectrum
*
      OPEN(22,file='../input/spectrum.in', status='old')
      READ(22,*)msta,mend,mstep,tmax,tb,kw1,kw2,z,ecc
      READ(22,*)neta,bwind,hewind,alpha1,lambda
      READ(22,*)ceflag,tflag,ifflag,wdflag,bhflag,nsflag,piflag,
     &       spinflag,mxns,idum
      READ(22,*)pts1,pts2,pts3
      READ(22,*)sigma1,sigma2,beta,xi,acc2,epsnov,eddfac,gamma

c      CALL date_and_time( date, time, zone)
c      write(*,*)date,time
c      time = time(1:6)
c:w      write(*,*)date,time
*
* Set the seed for the random number generator. 
*
      if(idum.gt.0) idum = -idum
*
* Set the collision matrix.
*
      CALL instar

      label(1) = 'INITIAL '
      label(2) = 'KW CHNGE'
      label(3) = 'BEG RCHE'
      label(4) = 'END RCHE'
      label(5) = 'CONTACT '
      label(6) = 'COELESCE'
      label(7) = 'COMENV  '
      label(8) = 'GNTAGE  '
      label(9) = 'NO REMNT'
      label(10) = 'MAX TIME'
      label(11) = 'DISRUPT '
      label(12) = 'BEG SYMB'
      label(13) = 'END SYMB'
      label(14) = 'BEG BSS'
*
* Open the output files. 
*
      OPEN(11,file='../params_spec.out',status='unknown')
      OPEN(12,file='../spectrum.out',status='unknown')
      WRITE(12,*)'# mzams[1]   mrem[2]   krem[3]   mfin[4]',
     &    '   kfin[5]   t[6]   tfin[7]'
*
      WRITE(11,1)msta,mend,mstep,tmax,tb,kw1,kw2,z,ecc
      WRITE(11,2)neta,bwind,hewind,alpha1,lambda
      WRITE(11,3)ceflag,tflag,ifflag,wdflag,bhflag,nsflag,piflag,
     &     spinflag,mxns,-idum
      WRITE(11,4)pts1,pts2,pts3
      WRITE(11,5)sigma1,sigma2,beta,xi,acc2,epsnov,eddfac,gamma
 1    FORMAT(2f6.1,f5.3,f8.1,f7.2,2i3,f7.4,f6.3)
 2    FORMAT(5f5.2)
 3    FORMAT(8i2,2x,f5.1,i7)
 4    FORMAT(3f5.2)
 5    FORMAT(2f6.1,2f5.1,f6.3,f5.1,f5.1)
*
* INPUT 
*
      if(mend.ge.150d0) WRITE(*,*) 'The upper mass limit is to high'
      nmass = (mend - msta)/mstep
*
      do i = 1,INT(nmass)

         m1 = msta + i*mstep
         m2 = 0*m1
         CALL zcnsts(z,zpars)
*
* Initialize the binary. 
*
         kstar(1) = kw1
         mass0(1) = m1
         mass(1) = m1
         massc(1) = 0.0d0
         ospin(1) = 0.0d0
         epoch(1) = 0.0d0
*
         kstar(2) = kw2
         mass0(2) = m2
         mass(2) = m2
         massc(2) = 0.0d0
         ospin(2) = 0.0d0
         epoch(2) = 0.0d0
**
         tphys = 0.0d0
         tphysf = tmax
         dtp = 0.0d0
*
* Evolve the binary. 
*
         CALL evolv2(kstar,mass0,mass,rad,lum,massc,radc,
     &               menv,renv,ospin,epoch,tms,
     &               tphys,tphysf,dtp,z,zpars,tb,ecc)
*
* Search the BCM array for the formation of binaries of 
* interest (data on unit 12 if detected) and also output. 
*
         jj = 0 
 30      jj = jj + 1
c         if(bcm(jj,1).eq.0.d0) goto 40
         if(bcm(jj,2).ge.10) goto 40
             mfin = bcm(jj,4)
             kfin = INT(bcm(jj,2))
             tfin = bcm(jj,1)
         goto 30
*
c 40      write(12,*) m1,10**bcm(jj-1,6)
 40       write(12,99) m1,bcm(jj,4),INT(bcm(jj,2)), mfin, kfin,
     &         bcm(jj,1),tfin
 99       format(2f10.4,i3,f10.4,i3,2f10.1)
*
      enddo
*
      CLOSE(12)
*
************************************************************************
*
      STOP
      END
***
