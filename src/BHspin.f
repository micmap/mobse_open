*** MM+RR did on July 2021
      SUBROUTINE BHspin(aspin, mc, z)
      implicit none

      INTEGER ceflag,tflag,ifflag,nsflag,wdflag
      INTEGER piflag,spinflag,baveraflag
      COMMON /FLAGS/ ceflag,tflag,ifflag,nsflag,wdflag
      COMMON /NEWFLAGS/ piflag,spinflag,baveraflag
      INTEGER idum
      COMMON/VALUE3/ idum
      INTEGER idum2,iy,ir(32)
      COMMON/RAND3/ idum2,iy,ir
      
      real*8 aspin, mc, z
      real*8 m1b,m2b,alow
      real*8 a,b
      real*8 a1,a2,b1,b2
      real*8 z1,z2,z3,z4,sigma,pi,twopi
      real*8 r1,r2,theta1,theta2,ss,zz
      real*8 x,y,w,v
      real ran3
      external ran3

      if(aspin.le.0.0)then
         if(spinflag.eq.0)then
            aspin=0.0
         elseif(spinflag.eq.1)then      
*     Geneva Models from Belczynski+2020
            a=-0.088

            if(z.ge.0.010)then
               b=2.258
               m1b=16.0
               m2b=24.2
               alow=0.13
            elseif(z.ge.0.004.and.z.lt.0.010)then
               b=3.578
               m1b=31.0
               m2b=37.8
               alow=0.25
            elseif(z.ge.0.0012.and.z.lt.0.004)then
               b=2.434
               m1b=18.0
               m2b=27.7
               alow=0.0
            else
               b=3.666
               m1b=32.0
               m2b=38.8
               alow=0.25
            endif

            if(mc.le.m1b)then
               aspin=0.85
            elseif(mc.gt.m1b.and.mc.lt.m2b)then
               aspin=a*mc+b
            elseif(mc.ge.m2b)then
               aspin=alow
            endif

         elseif(spinflag.eq.2)then
*     Mesa Models from Belczynski+2020

            if(z.ge.0.010)then
               a1=-0.0016
               b1=0.115
               aspin=a1*mc+b1
            elseif(z.ge.0.004.and.z.lt.0.010)then
               a1=-0.0006
               b1=0.105
               aspin=a1*mc+b1
            elseif(z.ge.0.0012.and.z.lt.0.004)then
               a1= 0.0076
               b1= 0.050
               a2=-0.0019
               b2=0.165
               m1b=12.09
               if(mc.le.m1b)then
                  aspin=a1*mc+b1
               else
                  aspin=a2*mc+b2
               endif
            else
               a1=-0.0010
               b1=0.125
               aspin=a1*mc+b1
            endif
         
         elseif(spinflag.eq.3)then
*     Fuller Model from Belczynsky+2020
            aspin=0.01

         elseif(spinflag.eq.4)then
*     Maxwelliana WIP

            pi=ACOS(-1.d0)
            twopi=2.d0*pi
            sigma=0.1
            z1=RAN3(idum)
            z2=RAN3(idum)
            z3=RAN3(idum)
            z4=RAN3(idum)
*           way to avoid negative values in LOG 
            ss=1.d0 -z1
            zz=1.d0 -z3
            if(ss.le.0.d0)then
               ss=1.d-6
            endif
            if(zz.le.0.d0)then
               zz=1.d-6
            endif

            r1=SQRT(-2.0*(sigma**2)*LOG(1-z1))
            theta1=twopi*z2
            r2=SQRT(-2.0*(sigma)**2*LOG(1-z3))
            theta2=twopi*z4

            x=r1*COS(theta1)
            y=r1*SIN(theta1)
            w=r2*COS(theta2)
            v=r2*SIN(theta2)

            aspin=SQRT(x**2+y**2+w**2)

         else
            WRITE(*,*) "ERROR: no spinflag set"
         endif
      else
         WRITE(*,*) "Bavera spin up", aspin
         OPEN(75,file='./bavera_spin_check.out', status='unknown')
         WRITE(75,*)aspin
      endif

      if(aspin.ge.1.0)then
         aspin=0.999
      endif
      if(aspin.lt.0.0)then
         aspin=0.0
      endif

      OPEN(55,file='./temporary_spin.out', status='unknown')
      WRITE(55,*)z,mc,aspin
      return
      end
