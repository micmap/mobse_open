#!/bin/bash

#common envelope alpha param of simulation to run
alpha=1.0
#metallicity of the simulations
met=0.002
#define the sn mechanism (1 startrack, 2 rapid, 3 delayed)
sn=3
#define kick type (2 fryer et al, 3 giacobbo&mapelli2020, 4 full kick)
bhflag=3
#kick Maxwellian rms km/s
kick=265.0

home=$PWD
echo $home

code=src #directory with your source code          
target=output #directory where you want to run and store output                 

mkdir $target
   
dir=$home/$target/A$alpha; echo $dir
#check if the folder already exists 
if [ -d $dir ]
then
    rm -r $dir/*
else 
    mkdir $dir
fi
  


mkdir $dir/$met/; cd $dir/$met
      

mkdir input src
          
#save the value of all parameters and cp all the input files
touch $dir/$met/input/popsyn_const.in
echo "0.5 0.0 1.0 ${alpha} 0.1
0 1 0 1 ${bhflag} ${sn} 1 4 0 3.0 29769
0.05 0.01 0.02
${kick} ${kick} 0.125 1.0 1.5 0.001 1.0 -1.0


# neta  bwind  hewind  alpha1  lambda
# ceflag  tflag  ifflag  wdflag  bhflag  nsflag piflag spinflag baveraflag mxns  idum
# pts1  pts2  pts3
# sigma1  sigma2  beta  xi  acc2  epsnov  eddfac  gamma" > $dir/$met/input/popsyn_const.in
cp $home/input/const_mobse.h      $dir/$met/input/
cp $home/input/zdata.h            $dir/$met/input/ 
cp $home/input/popsyn.in  $dir/$met/input/popsyn.in

#compile the source code
cd $home/$code/
echo $home/$code/
make popsyn
mv popsyn.x $dir/$met/src/popsyn${alpha}_${met}

#run mobse
cd $dir/$met/src/
./popsyn${alpha}_${met} > "${alpha}_${met}.log" 

