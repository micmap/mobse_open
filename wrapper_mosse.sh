#!/bin/bash


#list of met
metFile=$1

#list of masses
listmass=$2

#CE parameter
alpha=1.0

#define the sn mechanism (1 startrack, 2 rapid, 3 delayed)
sn=2

#define kick type (2 fryer et al, 3 giacobbo&mapelli2020, 4 full kick)
bhflag=3

#kick Maxwellian rms km/s
kick1=265.0
kick2=265.0

home=$PWD
echo $home

mobse=./
code=$mobse/src          
target=output/                 
echo

#compile sse
cd $home/$code/
echo $home/$code/
make mosse
cd $home/



dir=$home/$target/; echo $dir
#check if the folder already exists 
if [ -d $dir ]
then
    rm -r $dir/*
else 
    mkdir $dir
fi


#loop over the metallicities
while read -r linemet
do
    met=$linemet; echo $met
    mkdir $dir/$met/; 

    while read -r linemass
    do
	mas=$linemass; echo $mas
	mkdir $dir/$met/$mas/; cd $dir/$met/$mas
        mkdir input src
          
        #save the value of all parameters and cp all the input files
        touch input/single.in
        echo "${mas} ${met} 15000.
0.5 0.0 1.0 ${kick1} ${kick2}
0 1 ${bhflag} ${sn} 1 4 3.0 29769
0.05 0.01 0.02

"  > input/single.in
          cp $home/$mobse/input/const_mobse.h      input/
          cp $home/$mobse/input/zdata.h            input/ 

          #recompile the source code if needed
          cd $home/$code/
	  echo $home/$code/
          #make mosse
          cp mosse.x $dir/$met/$mas/src/mosse_${met}_${mas}

          #launch the sbatch command that runs mobse
          cd $dir/$met/$mas/src/
          ./mosse_${met}_${mas}
	  cp -i evolve.dat ../../../evolve_${met}_${mas}.dat
    done < $listmass
   
    cd $home

done < $metFile
