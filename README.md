# MOBSE + MOSSE with spins

This is the repository of the new version of MOBSE & MOSSE with black hole spins. Main developers: Nicola Giacobbo, Giuliano Iorio, Michela Mapelli and Mario Spera.



************************************************************************

## Information on the MOBSE+MOSSE package


	   
MOBSE is described in the following papers:

*"Revising Natal Kick Prescriptions in Population Synthesis Simulations"* [Giacobbo N. & Mapelli M. 2020, ApJ, Vol, 891](https://ui.adsabs.harvard.edu/abs/2020ApJ...891..141G/abstract)

*"The progenitors of compact-object binaries: impact of metallicity, common envelope and natal kicks"*
 [Giacobbo N. & Mapelli M. 2018, MNRAS 480, 2011–2030](https://ui.adsabs.harvard.edu/abs/2018MNRAS.480.2011G/abstract)  

*"Merging black hole binaries: the effects of progenitor's  metallicity, mass-loss rate and Eddington factor"*
 [Giacobbo N., Mapelli M., Spera M. 2018, MNRAS 474, 2959–2974](https://ui.adsabs.harvard.edu/abs/2018MNRAS.474.2959G/abstract)  

*"The cosmic merger rate of stellar black hole binaries from the Illustris simulation"* [Mapelli et al. 2017, MNRAS 472, 2422-2435](https://ui.adsabs.harvard.edu/abs/2017MNRAS.472.2422M/abstract) 


MOBSE/MOSSE (Massive objects in binary star evolution) is an upgraded and custom version of BSE/SSE ([Hurley et al. 2002, MNRAS, 329, 897](https://ui.adsabs.harvard.edu/abs/2002MNRAS.329..897H/abstract)).



If you need more information, please contact Michela Mapelli (michela.mapelli@unipd.it).


## Other papers using MOBSE

*"The cosmic evolution of binary black holes in young, globular, and nuclear star clusters: rates, masses, spins, and mixing fractions"* [Mapelli M. et al. 2022, MNRAS, 511, 5797](https://ui.adsabs.harvard.edu/abs/2022MNRAS.511.5797M/abstract)

*"Compact object mergers in hierarchical triples from low-mass young star clusters"* [Trani A. A. et al. 2022, MNRAS, 511, 1362](https://ui.adsabs.harvard.edu/abs/2022MNRAS.511.1362T/abstract)

*"Clustering of Gravitational Wave and Supernovae events: a multitracer analysis in Luminosity Distance Space"* [Libanore S. et al. 2022, JCAP, 02, 003](https://ui.adsabs.harvard.edu/abs/2022JCAP...02..003L/abstract)

*"New insights on binary black hole formation channels after GWTC-2: young star clusters versus isolated binaries"* [Bouffanais Y. et al. 2021, MNRAS, 507, 5224](https://ui.adsabs.harvard.edu/abs/2021MNRAS.507.5224B/abstract)

*"Intermediate-mass black holes from stellar mergers in young star clusters"* [Di Carlo U. N. et al. 2021, MNRAS, 507, 5132](https://ui.adsabs.harvard.edu/abs/2021MNRAS.507.5132D/abstract)

*"Dynamics of binary black holes in low-mass young star clusters"* [Rastello S. et al. 2021, MNRAS, 507, 3612](https://ui.adsabs.harvard.edu/abs/2021MNRAS.507.3612R/abstract)

*"The impact of binaries on the evolution of star clusters from turbulent molecular clouds"* [Torniamenti S. et al. 2021, MNRAS, 507, 2253](https://ui.adsabs.harvard.edu/abs/2021MNRAS.507.2253T/abstract)

*"Constraining accretion efficiency in massive binary stars with LIGO -Virgo black holes"* [Bouffanais Y. et al. 2021, MNRAS, 505, 3873](https://ui.adsabs.harvard.edu/abs/2021MNRAS.505.3873B/abstract)

*"Hierarchical black hole mergers in young, globular and nuclear star clusters: the effect of metallicity, spin and cluster properties"* [Mapelli M. et al. 2021, MNRAS, 505, 339](https://ui.adsabs.harvard.edu/abs/2021MNRAS.505..339M/abstract)


*"The cosmic merger rate density of compact objects: impact of star formation, metallicity, initial mass function, and binary evolution"* [Santoliquido F. et al. 2021, MNRAS, 502, 4877](https://ui.adsabs.harvard.edu/abs/2021MNRAS.502.4877S/abstract)

*"Gravitational Wave mergers as tracers of Large Scale Structures" [Libanore S. et al. 2021, JCAP, 02, 035](https://ui.adsabs.harvard.edu/abs/2021JCAP...02..035L/abstract)

*"Binary black holes in young star clusters: the impact of metallicity" [Di Carlo U. N. et al. 2020, MNRAS, 498, 495](https://ui.adsabs.harvard.edu/abs/2020MNRAS.498..495D/abstract)

*"The impact of electron-capture supernovae on merging double neutron stars"*
 [Giacobbo N. & Mapelli M. 2019, MNRAS 482, 2234–2243](https://ui.adsabs.harvard.edu/abs/2019MNRAS.482.2234G/abstract)  







## The MOBSE package contains the following FORTRAN files 

IMPORTANT: many subroutines are named as in the BSE package but contain some 
          upgrades. They are identified by (*). There are some new 
          functions indicated by (+).

mobse.f       - Main routine for binary stars (*).
	      	Evolves a single binary and creates data files (similar to bse.f)
		
mosse.f       - Main routine for single stars (*).
	      	Evolves a single star and creates data files (similar to sse.f) 

popsyn.f      - Main routine to evolve a number of binary stars
	      	(similar to popbin.f) (*)
		
../input/const_bse.h
	      - parameter file (*) and it contains new parameters

../input/binary.in           - input file for mobse.f (*)

../input/single.in     	     - input file for mosse.f (*)

../input/popsyn_const.in     - input file for popsyn.f (*)

evolv2.f      - routine that evolves a binary star (*)

evolv1.f      - routine that evolves a single star (*)
		
comenv.f      - common envelope evolution (*)

corerd.f      - estimates the core radius of a giant-like star

deltat.f      - determines stellar evolution update timestep 

dgcore.f      - determines the outcome when two degenerate cores merge 

gntage.f      - calculates parameters of new star resulting from a merger 

hrdiag.f      - decides which evolution stage the star is currently at 
                and then the appropriate luminosity, radius
                and core mass are calculated (*) 

instar.f      - sets the collision matrix (*)

kick.f        - generates supernova kick and adjusts orbital parameters (*)

mix.f         - models stellar collisions (*)

mlwind.f      - contains the mass loss prescription (*) 

mrenv.f       - calculates envelope parameters

ran3.f        - random number generator

rl.f          - calculates Roche-lobe radius 

star.f        - derives the landmark timescales and luminosities 
                that divide the various evolution stages (*)
		
zcnsts.f      - sets all the constants of the formulae which depend on 
                metallicity (apart for stellar winds) (*) 

zdata.h       - contains all the coefficient values for zcnsts (*) 

zfuncs.f      - all the formulae as a collection of separate functions (*)


pisn.f        - contains the prescriptions for the pair-instability and the 
                pulsation-pair-instability (+)

eddington.f   - calculates the Eddington factor (+)

fallback.f    - computes the fallback factor (+)

BHspin.f      - calculates BH spin magnitude (+)

NSspin.f      - calculates NS spin magnitude (+, still WIP)

NSspinup.f    - calculates NS spin up (+, still WIP)

WRspinup.f    - calculates spin up of BH by tides, following Bavera+2021 (+)

and 

Makefile  - gfortran compiler. Use command

	    "make mobse" for binary star evolution
	    
	    "make mosse" for single star evolution
	    
	    "make popsyn" for evolving a set of binary stars (with minimal outputs)
	    
	    "make clean" to clean the compilation
************************************************************************

MOBSE works as BSE and can be used nearly in the same way. If you are familiar with BSE, the main routine mobse.f (for binary stars) or mosse.f (for single stars) is an example to show how evolv2.f (for binary stars) and evolv1.f (for single stars) should be used. 
If you have never used BSE, don't worry: The routines contain all original information and all the upgrades are commented. 


************************************************************************

## Definitions of the evolution types for the stars  


 KW

0 - deeply or fully convective low mass MS star

1 - Main Sequence star

2 - Hertzsprung Gap

3 - First Giant Branch

4 - Core Helium Burning

5 - First Asymptotic Giant Branch

6 - Second Asymptotic Giant Branch

7 - Main Sequence Naked Helium star

8 - Hertzsprung Gap Naked Helium star

9 - Giant Branch Naked Helium star

10 - Helium White Dwarf

11 - Carbon/Oxygen White Dwarf

12 - Oxygen/Neon White Dwarf

13 - Neutron Star

14 - Black Hole

15 - Massless Supernova




************************************************************************

## FURTHER CAVEATS

Stellar mass range: 0.1 -> 150 Msun 

Metallicity range:  0.0001 -> 0.03 (0.02 is solar) 

Period range:       all days 

Eccentricity Range: 0.0 -> 1.0

************************************************************************
